;
(defpackage :flower-home
  (:use :cl :common-lisp
        :cl-who :hunchentoot
        :parenscript :babel)
  (:nicknames :home)
  (:export :start-server
           :stop-server))

(in-package :flower-home)

(defvar *server-handler* nil)
;
(defmacro standard-page ((&key title) &body body)
  `(cl-who:with-html-output-to-string
       (common-lisp:*standard-output* nil :prologue t :indent t)
     (:html
      (:head
       (:meta :charset "gbk")
       (:meta :name "Author" :contect "田俊华")
       (:title ,title)
       (:link :type "text/css"
              :rel "stylesheet"
              :href "/css/flower.css"))
      (:body
       (:div :class "bg-div"
             (:ul :id "Nav"
                  (:li
                   (:a :href "/flower.html" "Home"))
                  (:li
                   (:a :href "/about.html" "About"))
                  (:li
                   (:a :href "/projects.html" "Projects"))
                  (:li
                   (:a :href "/contact.html" "Contact"))))
       (:div :class "content-div"
             ,@body)))))

(hunchentoot:define-easy-handler (flower-index-page :uri "/flower.html") ()
  (progn
    (setf (html-mode) :html5)
    (standard-page (:title "tiny flower")
      (:br)
      (:h1 "Welcome to flower page...")
      (:hr)
      (:p "We'll complement all contents later..."))))

(hunchentoot:define-easy-handler (flower-about-me :uri "/about.html") ()
  (progn
    (setf (html-mode) :html5)
    (standard-page (:title "About Me")
      (:br)
      (:h1 "Working Experience")
      (:hr)
      )))

(hunchentoot:define-easy-handler (flower-project-display :uri "/projects.html") ()
  (progn
    (setf (html-mode) :html5)
    (standard-page (:title "My Projects")
      (:br)
      (:h1 "Dong Fang Bao Yuan")
      (:hr)
      )))

(hunchentoot:define-easy-handler (flower-contact-info :uri "/contact.html") ()
  (progn
    (setf (html-mode) :html5)
    (standard-page (:title "Contact infomation")
      (:br)
      (:h1 "My name is junhua tian.")
      (:hr)
      )))

(defun start-server (port)
  (setq *server-handler* (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port port :document-root "e:/git-db/flower_page/"))))

(defun stop-server ()
  (hunchentoot:stop *server-handler*))
